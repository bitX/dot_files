
=========================================================
# ----------------- Alacritty ----------------------##
=======================================================

# Def.
Alacritty is a modern terminal emulator that comes with sensible defaults, but allows for extensive configuration. By integrating with other applications, rather than reimplementing their functionality, it manages to provide a flexible set of features with high performance. The supported platforms currently consist of BSD, Linux, macOS and Windows. In addition, it migrated to toml already...!

# Screenshots

![image](https://i.ibb.co/jzw6sLq/2024-09-30-1727715111-1902x1039-scrot.png)
![image](https://i.ibb.co/TrbQXjt/2024-09-30-1727716578-1920x1055-scrot.png)
