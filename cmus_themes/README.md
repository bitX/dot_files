########################################################################################
 ###                                 CMUS Themes                                  ###
##################################################################################

Hi guys, When you'd downloaded themes, move in .config/cmus/

Then, to choise the theme, only write --→ :colorescheme <theme_name> [or you can tab key...]


Good-bye, take care off and I hope you like my theme: my.theme


images:
![image](https://i.imgur.com/qhIsZfR.png)
![image](https://i.imgur.com/lynkQFH.png)
![image](https://i.imgur.com/EpS33g9.png)
