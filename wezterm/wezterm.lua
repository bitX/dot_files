local wezterm = require 'wezterm'

local config = {}

config.color_scheme = 'Visibone Alt. 2 (terminal.sexy)'
 

config.window_frame = {
  -- The font used in the tab bar.
  -- Roboto Bold is the default; this font is bundled
  -- with wezterm.
  -- Whatever font is selected here, it will have the
  -- main font setting appended to it to pick up any
  -- fallback fonts you may have used there.
  font = wezterm.font({family= 'Roboto', weight= 'Bold' }),

  -- The size of the font in the tab bar.
  -- Default to 10. on Windows but 12.0 on other systems
  font_size = 12.0,

  -- The config of the titlebar and button.
  inactive_titlebar_bg = '#353535',
  active_titlebar_bg = '#e1e9b7',
  inactive_titlebar_fg = '#962fbf',
  active_titlebar_fg = '#ffffff',
  inactive_titlebar_border_bottom = '#028900',
  active_titlebar_border_bottom = '#634832',
  button_fg = '#cccccc',
  button_bg = '#2b2042',
  button_hover_fg = '#ffffff',
  button_hover_bg = '#3b3052',
}

tab_bar_at_bottom = true
tab_bar_position = "Bottom"

-- You can specify some parameters to influence the font selection;
-- for example, this selects a Bold, Italic font variant.
font = wezterm.font(
    'Iosevka Nerd Font Mono',
    { stretch = 'Expanded', weight = 'Regular' }
)

-- This inactive pane
config.inactive_pane_hsb = {
  saturation = 0.2,
  brightness = 0.5,
}

-- This increases color saturation by 50%
config.foreground_text_hsb = {
  hue = 0.8,
  saturation = 0.3,
  brightness = 0.8,
}

config.window_background_opacity = 0.7
config.text_background_opacity = 0.3

-- How many lines of scrollback you want to retain per tab
config.scrollback_lines = 3500
-- Enable the scrollbar.
-- It will occupy the right window padding space.
-- If right padding is set to 0 then it will be increased
-- to a single cell width
config.enable_scroll_bar = true

return config
