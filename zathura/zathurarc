##############################################
# Zathura settings                        ###
############################################

set default-fg                "#4C4F69"
set default-bg 			          "#D7C6CF"

set completion-bg		          "#CCD0DA"
set completion-fg		          "#4C4F69"
set completion-highlight-bg	  "#7BC043"
set completion-highlight-fg	  "#F9F9F9"
set completion-group-bg		    "#CCD0DA"
set completion-group-fg		    "#1E66F5"

set statusbar-fg		          "#F9F9F9"
set statusbar-bg		          "#7BC043"

set notification-bg		        "#CCD0DA"
set notification-fg		        "#4C4F69"
set notification-error-bg	    "#CCD0DA"
set notification-error-fg	    "#D20F39"
set notification-warning-bg	  "#CCD0DA"
set notification-warning-fg	  "#FAE3B0"

set inputbar-fg			          "#4C4F69"
set inputbar-bg 		          "#CCD0DA"

set recolor-lightcolor		    "#EFF1F5"
set recolor-darkcolor		      "#4C4F69"

set index-fg			            "#4C4F69"
set index-bg			            "#EFF1F5"
set index-active-fg		        "#4C4F69"
set index-active-bg		        "#CCD0DA"

set render-loading-bg		      "#EFF1F5"
set render-loading-fg		      "#4C4F69"

set highlight-color		        "#CCD0DA"
set highlight-fg              "#EA76CB"
set highlight-active-color	  "#EA76CB"


# Clipboard
set selection-clipboard clipboard

# Search
set incremental-search true
set search-adjust true

set adjust-open width

# One page per row by default
set page-per-row 1

# Stop at page boundries
set scroll-page-aware "true"
set scroll-full-overlap 0.04
set scroll-step 100

# Zoom settings
set zoom-min 10
set guioptions ""

# Typography
set font "Fantasque Nerd Fond 11"

###########################################
# Zathura mappings                     ###
#########################################

# Zoom in/out
map z zoom in
map Z zoom-out

# Toggle mode
map D toggle_page_mode

# Scroll
map a scroll half-up
map d scroll half-down

# Fullscreen
map [normal]      <F11> toggle_fullscreen
map [fullscreen]  <F11> toggle_fullscreen

map [normal]      <C-r> reload
map [fullscreen]  <C-r> reload

# Go to start
map w goto top

# Print
map p print

